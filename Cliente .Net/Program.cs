﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Cliente.Net
{
    class Program
    {
        public static void Main(string[] args)
        {
            Llamada();
        }

        public static void Llamada()
        {
            int Timeout = 4000;
            string mensaje = string.Empty;
            try
            {
                HttpStatusCode codigoEstado;
                string Cuerpo = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><JFIY008.Execute xmlns=\"http://tempuri.org/action/\"><Z0e478nro xsi:type=\"xsd:string\">4537700230197112</Z0e478nro><Z0e476cod xsi:type=\"xsd:string\">10</Z0e476cod><Z0e479pgc xsi:type=\"xsd:short\">1</Z0e479pgc><Z0e479suc xsi:type=\"xsd:short\">23</Z0e479suc></JFIY008.Execute></soap:Body></soap:Envelope>";
                HttpWebRequest solicitud = (HttpWebRequest)WebRequest.Create("http://localhost:19178/");
                solicitud.Method = "POST";
                solicitud.ContentType = "application/soap+xml";
                solicitud.ContentLength = Cuerpo.Length;
                //solicitud.ContentLength = 100;
                //solicitud.ServicePoint.ConnectionLimit = 1; //TOOD: Ver que hace con estas peticiones 
                solicitud.ServicePoint.Expect100Continue = false;
                using (Stream datosStream = solicitud.GetRequestStream())
                {
                    if (Timeout > 0)
                    {
                        datosStream.WriteTimeout = Timeout;
                    }
                    datosStream.Write(Encoding.UTF8.GetBytes(Cuerpo), 0, Cuerpo.Length);
                }

                using (HttpWebResponse respuesta = (HttpWebResponse)solicitud.GetResponse())
                {
                    using (var streamRecibido = respuesta.GetResponseStream())
                    {
                        if (Timeout > 0)
                        {
                            streamRecibido.ReadTimeout = Timeout;
                        }
                        using (var lector = new StreamReader(streamRecibido))
                        {
                            mensaje = lector.ReadToEnd();
                        }
                    }
                    codigoEstado = respuesta.StatusCode;
                }
                //Thread.Sleep(100);
                Console.Write(codigoEstado.ToString());
                Console.ReadKey();
            }
            catch (WebException webEx)
            {
                HttpStatusCode codigoEstado = HttpStatusCode.ServiceUnavailable;
                if (webEx.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = webEx.Response as HttpWebResponse;
                    if (response != null)
                    {
                        codigoEstado = (HttpStatusCode)response.StatusCode;
                    }
                }
                Console.Write(webEx.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
